package com.psystems.garagelab.sb.hwsbbatchprocessing.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("customers")
@RestController
public class CustomerController {

    @Autowired
    private JobLauncher jobLauncher;

    @GetMapping("/greeting")
    public ResponseEntity<String> sayHelloTo(@RequestParam(value = "visitor", required = false) String visitor) {
        if (StringUtils.isBlank(visitor))
            return ResponseEntity.ok("Hello there! Whats'up?");
        else
            return ResponseEntity.ok(String.format("Hi, %s! How are you?", visitor));
    }
}
