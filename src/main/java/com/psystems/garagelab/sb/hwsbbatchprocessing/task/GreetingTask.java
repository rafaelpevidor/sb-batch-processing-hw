package com.psystems.garagelab.sb.hwsbbatchprocessing.task;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

//@Component
public class GreetingTask implements Tasklet {
    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        System.out.println("[Tasklet][GreetingTask] Hello there! Whats'up?");
        return RepeatStatus.FINISHED;
    }
}
