package com.psystems.garagelab.sb.hwsbbatchprocessing.listener;

import com.psystems.garagelab.sb.hwsbbatchprocessing.model.dto.CustomerRow;
import com.psystems.garagelab.sb.hwsbbatchprocessing.model.entity.CustomerEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.SkipListener;

public class TransactionImportSkipListener implements SkipListener<CustomerRow, CustomerEntity> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void onSkipInRead(Throwable throwable) {
        logger.warn("Line skipped on read", throwable.getMessage());
    }

    @Override
    public void onSkipInWrite(CustomerEntity customerEntity, Throwable throwable) {
        logger.warn("Bean skipped on write", throwable.getMessage());
    }

    @Override
    public void onSkipInProcess(CustomerRow customerRow, Throwable throwable) {
        logger.warn("Bean skipped on process", throwable.getMessage());
    }
}
