package com.psystems.garagelab.sb.hwsbbatchprocessing.listener;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class HwStepListener implements StepExecutionListener {
    
    @Override
    public void beforeStep(StepExecution stepExecution) {
        
        if (!isSmallTalkEnabled(stepExecution))
            return;
        
        System.out.println("[HwStepListener] beforeStep - Step name: "
                + stepExecution.getStepName());

        System.out.println("[HwStepListener] beforeStep - Step context: "
                + stepExecution.getExecutionContext().toString());

        stepExecution.getExecutionContext().put("stepStartDate", LocalDateTime.now().toString());

        System.out.println("<stepListener:beforeStep>");
        System.out.println("Yes, I can access the Job Execution context and read properties.");
        System.out.println("We are setting stuff up to greet '"
                + stepExecution.getJobExecution().getExecutionContext().getString("greetingsTo")
                + "'.");

        System.out.println("Another cool thing that I can do is read 'Application Parameters'.");
        System.out.println("Do you wanna know who is the Job Runner? I'll tell you... He is "
                + stepExecution.getJobExecution().getJobParameters().getString("jobRunner")
                + ".");
        System.out.println("</stepListener:beforeStep>");
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        if (isSmallTalkEnabled(stepExecution)) {
            System.out.println("[HwStepListener] afterStep - Step name: "
                    + stepExecution.getStepName());

            System.out.println("[HwStepListener] afterStep - Step context: "
                    + stepExecution.getExecutionContext().toString());

            System.out.println("[HwStepListener] afterStep - Start date: "
                    + stepExecution.getExecutionContext().getString("stepStartDate"));

            System.out.println("[HwStepListener] afterStep - End date: "
                    + LocalDateTime.now());
        }
        return ExitStatus.COMPLETED;
    }

    private boolean isSmallTalkEnabled(StepExecution stepExecution) {
        return "step1".equalsIgnoreCase(stepExecution.getStepName());
    }
}
