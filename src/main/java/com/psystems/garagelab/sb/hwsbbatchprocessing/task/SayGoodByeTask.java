package com.psystems.garagelab.sb.hwsbbatchprocessing.task;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

//@Component
public class SayGoodByeTask implements Tasklet {
    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        System.out.println("[Tasklet][SayGoodByeTask] Nice to meet you! See you later.");
        return RepeatStatus.FINISHED;
    }
}
