package com.psystems.garagelab.sb.hwsbbatchprocessing.model.entity;

import com.psystems.garagelab.sb.hwsbbatchprocessing.model.dto.CustomerRow;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class CustomerEntity implements Serializable {

    private CustomerEntity(CustomerRow customerRow) {
        setFullName(customerRow.getFirstName() + " " + customerRow.getLastName());
        setEmail(customerRow.getEmail());
        setGender(customerRow.getGender());
        setIpAddress(customerRow.getIpAddress());
    }

    public CustomerEntity() {}

    private Long id;
    private String fullName;
    private String email;
    private String gender;
    private String ipAddress;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public static CustomerEntity of(CustomerRow customerRow) {
        return new CustomerEntity(customerRow);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        CustomerEntity that = (CustomerEntity) o;

        return new EqualsBuilder().append(id, that.id).
                append(fullName, that.fullName).append(email, that.email)
                .append(gender, that.gender).append(ipAddress, that.ipAddress)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(fullName).append(email).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("fullName", fullName)
                .append("email", email)
                .append("gender", gender)
                .append("ipAddress", ipAddress)
                .toString();
    }
}
