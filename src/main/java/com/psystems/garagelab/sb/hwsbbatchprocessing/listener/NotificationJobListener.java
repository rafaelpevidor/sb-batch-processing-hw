package com.psystems.garagelab.sb.hwsbbatchprocessing.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Component
public class NotificationJobListener implements JobExecutionListener {

    private static Logger logger = LoggerFactory.getLogger(NotificationJobListener.class);

    private Set<String> emailsToNotify;

    @Override
    public void beforeJob(JobExecution jobExecution) {
        logger.info("Loading watchers emails... of " + jobExecution.getJobInstance().getJobName());
        getEmails().add("7f2edff643-1551af@inbox.mailtrap.io");
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        logger.info("Sending notification email to [" + getEmails().size() + "] watchers to tell them the "
                + (BatchStatus.FAILED.equals(jobExecution.getStatus())?"bad news.":"news."));
    }

    private Set<String> getEmails() {
        if (Objects.isNull(emailsToNotify))
            emailsToNotify = new HashSet<>();
        return emailsToNotify;
    }
}
