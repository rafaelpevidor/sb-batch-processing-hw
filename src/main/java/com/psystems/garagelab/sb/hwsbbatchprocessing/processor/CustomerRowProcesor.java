package com.psystems.garagelab.sb.hwsbbatchprocessing.processor;

import com.psystems.garagelab.sb.hwsbbatchprocessing.model.dto.CustomerRow;
import com.psystems.garagelab.sb.hwsbbatchprocessing.model.entity.CustomerEntity;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class CustomerRowProcesor implements ItemProcessor<CustomerRow, CustomerEntity> {

    @Override
    public CustomerEntity process(CustomerRow customerRow) throws Exception {
        Validate.isTrue(Objects.nonNull(customerRow), "Customer row can't be null");
        Validate.isTrue(StringUtils.isNotBlank(customerRow.getEmail()), "Customer email can't be null");
        Validate.isTrue(StringUtils.isNotBlank(customerRow.getFirstName()), "Customer first name can't be null");
        Validate.isTrue(StringUtils.isNotBlank(customerRow.getLastName()), "Customer last name can't be null");
        return CustomerEntity.of(customerRow);
    }
}
