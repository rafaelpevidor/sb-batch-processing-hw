package com.psystems.garagelab.sb.hwsbbatchprocessing.processor;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class SumItemProcessor implements ItemProcessor<Integer, Integer> {
    @Override
    public Integer process(Integer element) throws Exception {
        return Integer.sum(10, element);
    }
}
