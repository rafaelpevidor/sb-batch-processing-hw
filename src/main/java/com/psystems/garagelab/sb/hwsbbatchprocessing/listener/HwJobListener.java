package com.psystems.garagelab.sb.hwsbbatchprocessing.listener;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class HwJobListener implements JobExecutionListener {
    @Override
    public void beforeJob(JobExecution jobExecution) {
        System.out.println("[HwJobListener] beforeJob - Job name: "
                + jobExecution.getJobInstance().getJobName());

        System.out.println("[HwJobListener] beforeJob - Job context: "
                + jobExecution.getExecutionContext().toString());

        jobExecution.getExecutionContext().put("jobStartDate", LocalDateTime.now().toString());
        jobExecution.getExecutionContext().put("greetingsTo", "Developer".toString());
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        System.out.println("[HwJobListener] afterJob - Job name: "
                + jobExecution.getJobInstance().getJobName());

        System.out.println("[HwJobListener] afterJob - Job context: "
                + jobExecution.getExecutionContext().toString());

        System.out.println("[HwJobListener] afterJob - Start date: "
                + jobExecution.getExecutionContext().getString("jobStartDate"));

        System.out.println("[HwJobListener] afterJob - End date: "
                + LocalDateTime.now());
    }
}
