package com.psystems.garagelab.sb.hwsbbatchprocessing.writer;

import org.springframework.batch.item.support.AbstractItemStreamItemWriter;

import java.io.Serializable;
import java.util.List;

public class ConsoleItemWriter extends AbstractItemStreamItemWriter<Serializable> {
    @Override
    public void write(List<? extends Serializable> list) throws Exception {
        System.out.println("Init of chunk...");
        list.stream().forEach(System.out::println);
        System.out.println("End of chunck.");
    }
}
