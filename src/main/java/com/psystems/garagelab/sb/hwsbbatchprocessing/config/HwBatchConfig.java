package com.psystems.garagelab.sb.hwsbbatchprocessing.config;

import com.psystems.garagelab.sb.hwsbbatchprocessing.listener.HwJobListener;
import com.psystems.garagelab.sb.hwsbbatchprocessing.listener.HwStepListener;
import com.psystems.garagelab.sb.hwsbbatchprocessing.listener.NotificationJobListener;
import com.psystems.garagelab.sb.hwsbbatchprocessing.listener.TransactionImportSkipListener;
import com.psystems.garagelab.sb.hwsbbatchprocessing.model.dto.CustomerRow;
import com.psystems.garagelab.sb.hwsbbatchprocessing.model.entity.CustomerEntity;
import com.psystems.garagelab.sb.hwsbbatchprocessing.processor.CustomerRowProcesor;
import com.psystems.garagelab.sb.hwsbbatchprocessing.processor.SumItemProcessor;
import com.psystems.garagelab.sb.hwsbbatchprocessing.reader.HwInMemoryItemReader;
import com.psystems.garagelab.sb.hwsbbatchprocessing.task.GreetingTask;
import com.psystems.garagelab.sb.hwsbbatchprocessing.task.SayGoodByeTask;
import com.psystems.garagelab.sb.hwsbbatchprocessing.writer.ConsoleItemWriter;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

@EnableBatchProcessing
@Configuration
public class HwBatchConfig {

    @Autowired
    private JobBuilderFactory jobs;

    @Autowired
    private StepBuilderFactory steps;

    @Autowired
    private HwJobListener hwJobListener;

    @Autowired
    private HwStepListener hwStepListener;

    @Autowired
    private SumItemProcessor sumItemProcessor;

    @Autowired
    private NotificationJobListener notificationJobListener;

    @Bean
    public ItemReader reader() {
        return new HwInMemoryItemReader();
    }

    //Hello World Job and Steps
    @Bean
    public Job firstJob() {
        return jobs.get("job1")
                //this class, RunIdIncrementer, is responsible to add run.id job parameter and increment it in each execution
                .incrementer(new RunIdIncrementer())//Make your job parameter different each time
                .listener(hwJobListener)
                .start(sayHello())
                .next(doSomethingWithNumbers())
                .next(sayGoodBye())
                .build();
    }

    @Bean
    public Step sayHello() {
        return steps.get("step1")
                .listener(hwStepListener)
                .tasklet(greetingsTasklet())
                .build();
    }

    @Bean
    public Step sayGoodBye() {
        return steps.get("step3")
                .listener(hwStepListener)
                .tasklet(sayGoodByeTasklet())
                .build();
    }

    public Step doSomethingWithNumbers() {
        return steps.get("step2")
                .<Integer, Integer>chunk(2)
                .reader(reader())//bean
                .processor(sumItemProcessor)//injection
                .writer(new ConsoleItemWriter())//new instance
                .build();
    }

    //A bit more sophisticated Job and Step
    @Bean
    public Job importCustomersJob() {
        return jobs.get("ImportCustomersFromCSV")
                .incrementer(new RunIdIncrementer())
                .listener(notificationJobListener)
                .start(importCustomersStep())
                .build();
    }

    @Bean
    public Step importCustomersStep() {
        return steps.get("ImportCustomersFromCSV-Step")
                .<CustomerRow, CustomerEntity>chunk(100)
                .reader(flatFileItemReader(null))
                .processor(new CustomerRowProcesor())
                .writer(new ConsoleItemWriter())
                .faultTolerant()
                .skipLimit(3)//If there is no limit for skips, then use skipPolicy instead
                .skip(IllegalArgumentException.class)
                .listener(new TransactionImportSkipListener())
                .build();
    }

//    You must use “@StepScope”, to indicate that the bean should be re-created for every new step.
//    Otherwise, the bean will be a global singleton and will not able able to see the parameters.
    @StepScope
    @Bean
    public FlatFileItemReader flatFileItemReader(
            @Value( "#{jobParameters['filePath']}" )
                    FileSystemResource inputFile ){
        FlatFileItemReader reader = new FlatFileItemReader();
        // step 1 let reader know where is the file
        reader.setResource(inputFile);

        //create the line Mapper
        reader.setLineMapper(
                new DefaultLineMapper<CustomerRow>(){
                    {
                        setLineTokenizer( new DelimitedLineTokenizer() {
                            {
                                setNames( new String[]{"id","first_name","last_name","email","gender", "ip_address"});
//                                setDelimiter(","); the default delimiter is comma.
                            }
                        });

                        setFieldSetMapper( new BeanWrapperFieldSetMapper<CustomerRow>(){
                            {
                                setTargetType(CustomerRow.class);
                            }
                        });
                    }
                }

        );
        //If the file have a header, then skip it
        //step 3 tell reader to skip the header
        reader.setLinesToSkip(1);
        return reader;
    }

    private Tasklet greetingsTasklet() {
        return new GreetingTask();
    }

    private Tasklet sayGoodByeTasklet() {
        return new SayGoodByeTask();
    }

}
