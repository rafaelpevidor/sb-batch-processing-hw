package com.psystems.garagelab.sb.hwsbbatchprocessing.reader;

import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.support.AbstractItemStreamItemReader;

public class HwInMemoryItemReader extends AbstractItemStreamItemReader<Integer> {

    private static final Integer[] ELEMENTES = {0,1,2,3,4,5,6,7,8,9,10};

    private int index = 0;

    @Override
    public Integer read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        if (index < ELEMENTES.length) {
            return ELEMENTES[index++];
        }
        return null;
    }
}
